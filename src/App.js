// import logo from './logo.svg';
import './App.css';
// import { Fragment } from 'react';
import { Container } from "react-bootstrap"
import { BrowserRouter as Router} from "react-router-dom"
import { Route, Routes } from "react-router-dom"
// import Banner from './components/Banner';
// import Highlight from "./components/Highlight";
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses';
import Home from "./pages/Home";
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error";
import { UserProvider } from './pages/UserContext';
import { useState, useEffect, useContext } from 'react';
import Settings from "./pages/Settings";
import CourseView from './pages/CourseView';

function App() {

  // 1. Create State 
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  
  return (
    /* React Fragments allows us to return multiple elements */

    // 2. Provide/share the state to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
    
        <Container fluid>
          {/* <Banner />
          <Highlight /> */}
          <AppNavBar />
          <Routes>
            <Route path= "/" element = {<Home />}/>
            <Route path= "/courses" element = {<Courses />}/>
            <Route path= "/courses/:courseId" element = {<CourseView />}/>
            <Route path= "/register" element = {<Register />}/>
            <Route path= "/login" element = {<Login />}/>
            <Route path= "/logout" element = {<Logout />}/>
            <Route path= "/settings" element = {<Settings />}/>
            <Route path= "*" element = {<Error />} /> 
          </Routes>
        </Container>
    </Router>

  {/*  <Router>
      <Routes>
        <Route path = "/yourDesiredPath" element = {} />
      </Routes>
    </Router> */}
    </UserProvider>


  );
}

export default App;
