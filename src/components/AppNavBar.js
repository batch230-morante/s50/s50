/* import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav"; */
import { Container, Navbar, Nav} from "react-bootstrap"
import {Link, NavLink} from "react-router-dom"; 
import { useContext, useState, Fragment } from "react";
import UserContext from "../pages/UserContext";

export default function AppNavBar(){
	// const [user, setUser] = useState(localStorage.getItem("email"));

	// s55
	const { user } = useContext(UserContext);
	// console.log("result of getting item from localstorage: ");
	console.log(user);


    return(
    <Navbar bg="light" expand="lg">
		<Container fluid>
			<Navbar.Brand>Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav"/>
		<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="mr-auto">
			<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			<Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>
			{(user.id !== null)?
				<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
				:
			<Fragment>
				<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
				<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
			</Fragment>
			}
			
			</Nav>
		</Navbar.Collapse>
	</Container>
		</Navbar>
    )
}
