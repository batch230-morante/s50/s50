import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){

    // console.log("Contents of props: ");
    // console.log(props);
    // console.log(typeof props);

    const {_id, name, description, price} = courseProp;
// State Hooks (useState) - a way to store information within a component and track this information
    // getter, setter
    // variable, function to change the value of a variable
/* const [count, setCount] = useState(0); // count = 0;
const [takenSlot, setSlot] = useState(30); */

// function enroll(){
//     /*  if(takenSlot > 0){
//     setCount(count + 1);
//     setSlot(takenSlot - 1);
//     // setSlot
//     console.log("Enrollees: " + count);
//     }
//     else{
//         alert("No more seats");
//     } */
//     setCount(count + 1);
//     setSlot(takenSlot - 1);
//     // setSlot
//     console.log("Enrollees: " + count);
//     console.log("Seats: "+ takenSlot)
// }

// useEffect() always runs the task on the initial render and/or every render (when the state changes in a component)
// Initial render is when the component is ran or displayed for the first time
/* useEffect(() => {
    if(takenSlot === 0){
        alert("No more seats available");
    }
}, [takenSlot]); */

    return(
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                    <Card.Subtitle><b>Description:</b></Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                    <Card.Subtitle><b>Price:</b></Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button as = {Link} to = {`/courses/${_id}`} variant="primary">Enroll</Button>
                {/* <Card.Text>Total Enrolled: {count}</Card.Text> */}
            </Card.Body>
        </Card>
    );
}
/* 
CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
} */