import React from 'react';
import ReactDOM from 'react-dom/client';
// import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
import "bootstrap/dist/css/bootstrap.min.css";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

/* const name = "John Smith";
const user = {
  firstName: "Jane",
  lastName: "Smith"
}

function formatName(parameterUser){
  return parameterUser.firstName + " " + parameterUser.lastName;
}

const element = <h1>Hello, {formatName(user)}</h1>

// We need to create a root to display React components
  // Reference: https://beta.reactjs.org/reference/react-dom/client/createRoot
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(element); */


// STARTING STARTING STARTING
/*
  option 1 preparations:
  - npx create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap

  option 2 preparations
  - npm install -g create-react-app
  - create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap
*/