import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useReducer, useContext } from "react";
import UserContext from "../pages/UserContext";
import { Navigate } from "react-router-dom";
import Swol from "sweetalert2";

{/* <h1>Login</h1> */}
export default function Login(){

    // 3. Use the State
    // Variable, setter function
    const { user, setUser } = useContext(UserContext);

    // State hooks to store values of the input fields
    const [email, setEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

/* function authenticate(e){
    e.preventDefault();
    localStorage.setItem("email", email);

    setUser({
        email: localStorage.setItem("email", email)
    })

    setEmail("");
    setLoginPassword("");

    console.log(`${email} has been verified! Welcome back!`);
    alert("Successfully logged in")
};
 */

    /* function loginUser(event){
        event.preventDefault();
        setEmail("");
        setLoginPassword("");
        alert("Thank you for registering!")
    } */


    function authenticate (e){
        e.preventDefault();

        fetch("http://localhost:4000/users/login", {
            method: "POST",
            headers: {"Content-type" : "application/json"},
            body: JSON.stringify({
                email: email,
                password: loginPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swol.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            }
            else{
                Swol.fire({
                    title: "Authentication failed.",
                    icon: "error",
                    text: "Check your login details and try again"
                })
            }
            
        })
        setEmail("");
    }



    const retrieveUserDetails = (token) => {
        fetch("http://localhost:4000/users/details", {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


function loginUser(event){
    localStorage.setItem("email", email);
    setEmail("");
    setLoginPassword("");

    console.log(`${email} has been verified! Welcome back!`);
    alert("Successfully logged in")
}
    useEffect(() => {
        if(email !== "" && loginPassword !== ""){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, loginPassword])



    return(
        (user.id !== null)
        ? // true - means email field is not null or has a value
        <Navigate to = "/courses" />
        : // false - means email field is not successfully set
        <Form onSubmit = {(event) => authenticate(event)}>
        <h3>Login</h3>
            <Form.Group controlId="loginEmail">
            <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                            />
            </Form.Group>

            <Form.Group controlId="loginPassword">
                <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Enter your password"
                        value = {loginPassword}
                        onChange = {event => setLoginPassword(event.target.value)}
                        required
                        />
            </Form.Group>
            {isActive ? 
                <Button variant="success" type="submit" id="submitBtn">
                    Login
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Login
                </Button>
                }
        </Form>
    )

}