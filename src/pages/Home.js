import { Fragment, fragment } from "react";
import Banner from "../components/Banner";
import Highlight from "../components/Highlight";
// import CourseCard from "../components/CourseCard";

const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/",
    label: "Enroll Now!"
}

export default function Home(){
    
    return (
        <Fragment>
            <Banner data = {data}/>
            <Highlight />
            {/* <CourseCard /> */}
        </Fragment>
    )
}