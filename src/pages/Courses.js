// import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";
import { Fragment} from 'react';
import UserContext from "./UserContext"
import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";



export default function Courses(){

    /*  console.log("Contents of coursesData");
    console.log(coursesData);
    console.log(coursesData[0]); */
    
    const { user } = useContext(UserContext)

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        fetch(`http://localhost:4000/courses/`) // ${process.env.REACT_APP_API_URL} http://localhost:4000/courses/
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setCourses(data.map(course => {
                return(
                    <CourseCard key = {course._id} courseProp = {course}/>
                )
            }))
        })
    }, [])

    // array method to display all courses
/*    const courses = coursesData.map(course => {
        return(
            <CourseCard key = {course.id} courseProp = {course} />
        )
    }) */


    // return(
    //     <Fragment>
    //         <CourseCard courseProp = {coursesData[0]}/>
    //     </Fragment>
    // )
        return(
            (user.isAdmin)?
                <Navigate to = "/admin" />
                :
                <>
                    <h1> Courses </h1>
                    {courses}
                </>
        )
}