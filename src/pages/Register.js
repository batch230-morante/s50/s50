import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useReducer, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "./UserContext";
import Swal from "sweetalert2";

export default function Register(){

    const { user } = useContext(UserContext);
    // State hooks to store the values of input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // State to determine whether submit button us enabled or not
    const [isActive, setIsActive] = useState(false);
    const navigate = useNavigate();

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);



    useEffect(()=> {
        if((email !== "" && password1 !=="" && password2!== "") && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
            // alert("Both passwords must match")
        }
    }, [email, password1, password2])


/* // OG REGISTER FUNCTION BELOW ___   
    function registerUser(event){
        // Prevents page redirection via form submission
        event.preventDefault();

        //Clear input fields
        setEmail("");
        setPassword1("");
        setPassword2("");
        alert("Thank you for registering!")
    }
*/ //OG REGISTER FUNCTION ABOVE ^^^

const registerUser = (event) => {

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
        method: "POST",
        headers: {
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            firstName : firstName,
            lastName: lastName,
            email: email,
            password: password1,
            mobileNumber: mobileNumber
        })
    })
    .then(res => res.json())
    .then(data => {
    console.log("Register data");
    console.log(data);

    if(data){

        Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
        })
        setEmail("");
        setPassword1("");
        setPassword2("");
        navigate("/login")
    }
    else{
        Swal.fire({
            title: "Registration Failed",
            icon: "error",
            text: "Please provide a different email."
        })
    }
})
}



    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit = {(event) => registerUser(event)}>
            <Form.Group controlId="userFirstName">
            <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your first name"
                        value = {firstName}
                        onChange = {event => setFirstName(event.target.value)}
                        required/>
            </Form.Group>

            <Form.Group controlId="userLastName">
            <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your last name"
                        value = {lastName}
                        onChange = {event => setLastName(event.target.value)}
                        required/>
            </Form.Group>

            <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                            />
                    <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                    </Form.Text>
            </Form.Group>

            <Form.Group controlId="UserMobileNumber">
            <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your mobile number"
                        value = {mobileNumber}
                        onChange = {event => setMobileNumber(event.target.value)}
                        required/>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value = {password1}
                        onChange = {event => setPassword1(event.target.value)}
                        required
                        />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password"
                        value = {password2}
                        onChange = {event => setPassword2(event.target.value)}
                        required
                        />
            </Form.Group>
            { isActive ? //true
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : //false
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
                }
        </Form>
    )
}

